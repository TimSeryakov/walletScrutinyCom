---
permalink: /
title: "Is your Bitcoin wallet secure?"
excerpt: "Many wallets are not open to public scrutiny"
author_profile: true
redirect_from:
  - /about/
  - /about.html
---


**Wallet Scrutiny** is a project aimed at improving the security of
Android Bitcoin Wallets.


What protects your Bitcoins from Hackers?
============================

<img src="/images/hacker.jpg" alt="hacker" style="height:10em;float:left;margin:0 1em 1em 0" />
Do you own your Bitcoins or do you trust that your app allows you to use "your"
coins while they are actually controlled by "them"? Do you have a backup? Do
"they" have a copy they didn't tell you about? Did anybody check the wallet for deliberate backdoors
or vulnerabilities? Could anybody check the wallet for those?

We try to answer these questions for Bitcoin wallets on Android.

Read about [our methodology](/methodology/) to understand in more detail.

As featured on ...
==================

{% include press.html %}

All wallets ordered by verifiability, downloads and ratings
===========================================================

{% include list_of_wallets.html %}

How many wallets are in each category?
====================================

{% include grid_of_wallets.html %}

How many users (downloads) are in each category?
====================================

{% include grid_of_wallets_proportional.html %}
