---
layout: archive
title: "F-Droid Reproducible Builds"
permalink: /fdroid/
author_profile: true
---

For example [Bitcoin Wallet (Schildbach)](/schildbach/) claims to be
reproducible using `fdroid build --server de.schildbach.wallet`.
How does this actually work? Is this a good enough verification of the app?

We will explore these questions in this page.
