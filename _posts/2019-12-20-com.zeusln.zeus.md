---
title: "Zeus: Bitcoin/Lightning Wallet"
altTitle: 

users: 500
appId: com.zeusln.zeus
launchDate: 2019-03-16
latestUpdate: 2020-05-10
apkVersionName: "Varies with device"
stars: 4.7
ratings: 30
reviews: 21
size: Varies with device
website: undefined
repository: 
issue: 
icon: com.zeusln.zeus.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.zeusln.zeus/
redirect_from:
  - /com.zeusln.zeus/
---


