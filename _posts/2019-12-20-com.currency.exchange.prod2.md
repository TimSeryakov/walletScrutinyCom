---
title: "Currency.com Exchange"
altTitle: 

users: 10000
appId: com.currency.exchange.prod2
launchDate: 
latestUpdate: 2020-05-11
apkVersionName: "1.0.335"
stars: 4.3
ratings: 757
reviews: 265
size: Varies with device
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.prod2.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: currencycom
providerLinkedIn: company/currencycom/
providerFacebook: currencycom/
providerReddit: 

permalink: /posts/com.currency.exchange.prod2/
redirect_from:
  - /com.currency.exchange.prod2/
---


This is an interface for a custodial trading platform and thus **not
verifiable**.