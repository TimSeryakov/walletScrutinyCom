---
title: "Coinsquare"
altTitle: 

users: 10000
appId: coinsquare.io.coinsquare
launchDate: 
latestUpdate: 2020-05-11
apkVersionName: "2.13.6"
stars: 2.5
ratings: 308
reviews: 234
size: 8.2M
website: https://coinsquare.com/
repository: 
issue: 
icon: coinsquare.io.coinsquare.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: https://twitter.com/coinsquare
providerLinkedIn: 
providerFacebook: https://www.facebook.com/coinsquare.io/
providerReddit: 

permalink: /posts/coinsquare.io.coinsquare/
redirect_from:
  - /coinsquare.io.coinsquare/
---


This is the interface for an exchange. In the description we read:

> We are SSL and 2FA enabled, with a 95% cold storage policy on all digital
  currency, and run multiple encrypted and distributed backups every day.

which means this is a custodial service and thus **not verifiable**.