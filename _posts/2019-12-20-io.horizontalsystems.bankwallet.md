---
title: "UNSTOPPABLE - Bitcoin Wallet"
altTitle: 

users: 1000
appId: io.horizontalsystems.bankwallet
launchDate: 2018-12-18
latestUpdate: 2020-06-12
apkVersionName: "0.14.1"
stars: 4.5
ratings: 180
reviews: 154
size: 28M
website: https://unstoppable.money/
repository: https://github.com/horizontalsystems/unstoppable-wallet-android
issue: https://github.com/horizontalsystems/unstoppable-wallet-android/issues/2326
icon: io.horizontalsystems.bankwallet.png
bugbounty: 
verdict: reproducible # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-07
reviewStale: false
signer: c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
reviewArchive:
- date: 2020-06-07
  version: "0.14.0"
  apkHash: 9abbc4c1b7475c75437c416f5e103d4fd83625f0a7463be6aec545bff86d920d
  gitRevision: b5ebfb9fdedf9b686511645bae3e05fe13aa3d2f
  verdict: nonverifiable
- date: 2020-05-16
  version: "0.13.0"
  apkHash: 20b023949e0572af577beb0df94c6dbaf758be0d7bd323e632392c93c6640f2d
  gitRevision: f2664abad8e41ce1b3e225be0eae63d18a0cc053
  verdict: reproducible
- date: 2020-03-25
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 45359b810b471200750ab0914f6c506054cf1123
  verdict: nonverifiable
- date: 2020-03-21
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 65d19944379884fc3f0b9268e7e83b7dda63b5ba
  verdict: nonverifiable
- date: 2020-01-31
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 43d012f4990ca6fed9d4b042224bf8fdd48ff41e
  verdict: reproducible
- date: 2020-01-29
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 92e4a67ecc626220965114cd6a4cd67497e3be9f
  verdict: nonverifiable

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/io.horizontalsystems.bankwallet/
redirect_from:
  - /io.horizontalsystems.bankwallet/
---


The latest version on Google Play did as follows with our
[test script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh):

```
Results for 
appId:          io.horizontalsystems.bankwallet
signer:         c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
apkVersionName: 0.14.1
apkVersionCode: 28
apkHash:        a45881e3ae8bba31c2fc09ecbb63ce5d200c77512f256971a12e9ab830ae719d

Diff:
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_28/apktool.yml and /tmp/fromBuild_io.horizontalsystems.bankwallet_28/apktool.yml differ
```

This is what we want to see to consider the app **reproducible**.
