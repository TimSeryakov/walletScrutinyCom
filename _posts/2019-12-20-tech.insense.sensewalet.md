---
title: "iWallet - blockchain wallet for Bitcoin, Ethereum"
altTitle: 

users: 5000
appId: tech.insense.sensewalet
launchDate: 2018-09-27
latestUpdate: 2019-06-21
apkVersionName: "0.0068beta"
stars: 4.1
ratings: 19
reviews: 11
size: 14M
website: http://InSense.tech
repository: 
issue: 
icon: tech.insense.sensewalet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-30
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/tech.insense.sensewalet/
redirect_from:
  - /tech.insense.sensewalet/
---


In their description we read:

> *You, as owner of wallet, fully control the private key:*
  * "iWallet"does not store info about users, data about private keys, or funds
    on servers.
  * Private keys are stored on your device.

so they are non-custodial.

The following is their website in its entirety:

![InSense this website is under construction](/images/insenseunderconstruction.png)

So, trusting their claim from the play store, not finding any further data to
verify it, we remain with the verdict: **not verifiable**.